# coding: utf-8

from django.http import JsonResponse
from django.utils.translation import ugettext_lazy as _


def get_api_key(request):
    data = {}
    if request.user.is_authenticated():
        data['key'] = request.user.apidata.key
    else:
        data['error'] = _('You are not authorised.')
    return JsonResponse(data)
