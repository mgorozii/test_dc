# coding: utf-8

from django.contrib.auth import authenticate


class ApiAuthMiddleware(object):
    def process_request(self, request):
        if not request.user.is_authenticated():
            request.user = authenticate(request=request)
