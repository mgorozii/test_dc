# coding: utf-8

from django.conf.urls import url, include

from .views import get_api_key
from .api import ContactResource

urlpatterns = [
    url(r'api/contacts/', include(ContactResource.urls())),
    url(r'get_api_key', get_api_key)
]
