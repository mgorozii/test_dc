# coding: utf-8

from uuid import uuid4

from django.conf import settings
from django.db import models
from django.db.models import signals
from django.utils.translation import ugettext_lazy as _

from phonenumber_field.modelfields import PhoneNumberField


class Contact(models.Model):
    """ Model for storing contacts. """

    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    first_name = models.CharField(verbose_name=_('First Name'), max_length=150)
    last_name = models.CharField(verbose_name=_('Last Name'), max_length=150)
    phone = PhoneNumberField(verbose_name=_('Phone'))

    def __unicode__(self):
        return ' '.join((self.first_name, self.last_name))


def generate_key():
    """ Returns random 32 bytes string. """
    return uuid4().hex


class ApiData(models.Model):
    """ Extending User-model for storing api-data. """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    key = models.CharField(max_length=32, default=generate_key)


def create_api_data(sender, instance, created, **kwargs):
    """ Create ApiData for User"""
    if created:
        ApiData.objects.create(user=instance)


signals.post_save.connect(create_api_data, sender=settings.AUTH_USER_MODEL, weak=False,
                          dispatch_uid='models.create_model_b')
