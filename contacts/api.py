# coding: utf-8

from restless.dj import DjangoResource
from restless.preparers import FieldsPreparer

from .models import Contact


class ContactResource(DjangoResource):
    preparer = FieldsPreparer(fields={
        'id': 'id',
        'first_name': 'first_name',
        'last_name': 'last_name',
        'phone': 'phone.as_international',
    })

    def list(self, *args, **kwargs):
        return Contact.objects.filter(user=self.request.user)

    def create(self, *args, **kwargs):
        return Contact.objects.create(
                user=self.request.user,
                first_name=self.data['first_name'],
                last_name=self.data['last_name'],
                phone=self.data['phone'],
        )

    def update(self, *args, **kwargs):
        try:
            contact = Contact.object.get(id=id, user=self.request.user)
        except Contact.DoesNotExist:
            contact = Contact()

        contact.user = self.request.user if not contact.user else contact.user
        contact.first_name = self.data['first_name']
        contact.last_name = self.data['last_name']
        contact.phone = self.data['phone']

    def delete(self, pk):
        Contact.objects.get(id=pk, user=self.request.user).delete()

    def is_authenticated(self):
        return self.request.user.is_authenticated()
