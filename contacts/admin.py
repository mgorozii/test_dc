# coding: utf-8

from django.contrib import admin

from .models import Contact


class ContactsAdmin(admin.ModelAdmin):
    pass


admin.site.register(Contact, ContactsAdmin)
