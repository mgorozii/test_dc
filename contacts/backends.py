# coding: utf-8

from django.conf import settings

from .models import ApiData


class ApiAuthBackend(object):
    def authenticate(self, username=None, password=None, request=None):
        try:
            if not request:
                return

            return ApiData.objects.get(key=request.GET[settings.API_KEY_NAME]).user

        except (ApiData.DoesNotExist, KeyError):
            return None
